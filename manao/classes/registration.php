<?php

session_start();
 

?>

<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="css/reset.css">
	<link rel="stylesheet" href="css/css.css">
	<link rel="stylesheet" href="trackbar/trackbar.css" type="text/css">

	<script type="text/javascript" src="js/jquery-1.8.2.min.js" ></script>
	<script type="text/javascript" src="js/js.js" ></script>
	<script type="text/javascript" src="js/shop-script.js"></script>
	<script type="text/javascript" src="js/jquery.cookie.js" ></script>

	<script type="text/javascript" src="js/jquery.form.js" ></script>
	<script type="text/javascript" src="js/jquery.validate.js" ></script>
	<script type="text/javascript" src="js/jquery.validate.js" ></script>
    <script type="text/javascript" src="js/TextChange.js" ></script>


	<script></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#form_reg').validate({
				//правила для проверки что будет исполнять
				rules:{
					"reg_login":{
						required:true,
						minlength:5,
						maxlength:15,
						remote:{
							type: "post",
							url:"reg/check_login.php"
						       }
					},
					"reg_pass":{
						required:true,
						minlength:7,
						maxlength:15
					
					},
					
					"reg_name":{
						required:true,
						minlength:3,
						maxlength:15
					
						},
					
					"reg_captcha":{
						required:true,
						remote:{
							type: "post",
							url:"/reg/check_captcha.php"
						    }
					
						}
					},
					//сообщения  при нарушении правил
					messages:{
						"reg_login":{
							required:"Укажите Логин",
							minlength:"от 5 до 15 символов!!!",
							maxlength:"от 5 до 15 символов!!",
							remote:"Логин занят , позвоните попозже )))"
						},
						"reg_pass":{
							required:"Укажите Пароль",
							minlength:"от 7 до 15 символов!!!",
							maxlength:"от 7 до 15 символов!!"
					
						},
						
						"reg_name":{
							required:"Укажите Имя",
							minlength:"от 3 до 15 символов!!!",
							maxlength:"от 3 до 15 символов!!"
						
						
						},
						"reg_captcha":{
							required:"Введите код",
							minlength:"Не верный код проверки"
						
						}
					},
submitHandler: function(form){
	$(form).ajaxSubmit({
		success: function(data){

			if(data=='true')

			{
				$("#block-form-registration").fadeOut(300,function()
				{
					$("#reg_message").addClass("reg_message_good").fadeIn(400).html("Вы успешно зарегистированы");
					$("#form_submit").hide();
				});
			}
			else{
				$("#reg_message").addClass("reg_message_error").fadeIn(400).html(data);
			}
		}
	});
}
			
			});
		});
	</script>

	<title>Регистрация</title>
</head>
<body>
<div id="block-body">


</div>
<div id="block-content">
<h2 class="h2-title">Регистрация</h2>
<form method="post" id="form_reg" action="reg/handler_reg.php">
	
<p id="reg_message"></p>
<div id="block-form-registration">
	<ul id="form-registration">
		<li>
			<label >Логин</label>
			<span class="star">*</span>
			<input type="text" name="reg_login" id="reg_login">
		</li>

<li>
		<label >Пароль</label>
			<span class="star">*</span>
			<input type="text" name="reg_pass" id="reg_pass">
			<span id="genpass">Сгенерировать</span>
		</li>

		

		<li>
		<label >Имя</label>
			<span class="star">*</span>
			<input type="text" name="reg_name" id="reg_name">
		</li>

	
		<li>
			<div id="block-captcha">
				<img src="reg/reg_captcha.php" >
				<input type="text" name="reg_captcha" id="reg_captcha">
				<p id="reloadcaptcha">Обновить</p>

			</div>
		</li>
	</ul>
</div>
<p align="right"><input type="submit" name="reg_submin" id="form_submit" value="Регистрация"></p>
</form>

</div>





	</div>
</body>
</html>
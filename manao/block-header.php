<?php
session_start();
?>


<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="css/reset.css">
	<link rel="stylesheet" href="css/css.css">

	<link rel="shortcut icon" href="/favicon.ico">

	<script type="text/javascript" src="js/jquery-1.8.2.min.js" ></script>


	<script type="text/javascript" src="js/shop-script.js"></script>
	<script type="text/javascript" src="js/jquery.cookie.js" ></script>


	<script> $('.top-auth').toggle(
       function() {
           $(".top-auth").attr("id","active-button");
           $("#block-top-auth").fadeIn(200);
       },
       function() {
           $(".top-auth").attr("id","");
           $("#block-top-auth").fadeOut(200);  
       }
    );</script>

	<title>Регистрация</title>
</head>
<body>
<div id="block-body">
<div id="block-header"> 
<div id="header-top-block">
<ul id="header-top-menu"> <li>
	Ваш город <span>Москва</span>
</li>
<li><a href="o-nas.php">О нас </a></li>
<li><a href="magaziny.php">Магазины </a></li>
<li><a href="contacts.php">Контакты</a></li>

</ul>

<?php

if ($_SESSION['auth'] == 'yes_auth')
{
 
 echo '<p id="auth-user-info" align="right"><img src="/images/user.png" />Здравствуйте, '.$_SESSION['auth_name'].'!</p>';   
    
}else{
 
  echo '<p id="reg-auth-title" align="right"><a class="top-auth">Вход</a><a href="registration.php">Регистрация</a></p>';   
    
}
	
?>
<div id="block-top-auth">

	<div class="corner"></div>

		<form method="post">

			<ul id="input-email-pass">

				<h3 >Вход</h3>
				<p id="message-auth">Неверный логин и (или) пароль</p>
				<li>
					<center> <input type="text" id="auth_login" placeholder="Логин или Е-маил" ></center>
				</li>
				<li>
					<center> <input type="password" placeholder="Пароль" id="auth_pass"><span id="button-pass-show-hide" class="pass-show"></span></center>
				</li>
				<ul id="list-auth">
					<li><input type="checkbox" name="rememberme" id="rememberme"><label for="rememberme">Запомнить меня ?</label></li>
					<li><a href="#" id="remindpass">Забыли пароль?</a></li>

				</ul>
				<p align="right" id="button-auth"><a href="">Вход</a></p>

				<p align="right" class="auth-loading"><img src="../images/loading.gif" ></p>
			</ul>
		</form>

<div id="block-remind">
<h3>Восстановление <br /> пароля</h3>
<p id="message-remind" class="message-remind-success" ></p>
<center><input type="text" id="remind-email" placeholder="Ваш E-mail" /></center>
<p align="right" id="button-remind" ><a>Готово</a></p>
<p align="right" class="auth-loading" ><img src="/images/loading.gif" /></p>
<p id="prev-auth">Назад</p>
</div>

	
</div>
	
</div>
<div id="top-line"></div>

<div id="block-user" >
<div class="corner2"></div>
<ul>
<li><img src="/images/user_info.png" /><a href="profile.php">Профиль</a></li>
<li><img src="/images/logout.png" /><a id="logout" >Выход</a></li>
</ul>
</div>



<img  id="imglogo" src="../images/logo.png" alt="">
<div id="personal-info">
	<p align="right"> Звонок Бесплатный</p>
	<h2>8 (029) 693 41 29</h2>
	<img src="../images/phone-icon.png" alt="">
	<p align="right"> режим работы </p>
	<p align="right"> Будние дни с 9:00 до 21:00</p>
	<p align="right"> Выходные- суббота , воскресенье</p>
	<img src="../images/time-icon.png" alt="">

</div>
<div id="block-search"> 
<form action="search.php?g=" method="GET">
<span id="pro"></span>
<input type="submit"  id="button-search" value="Поиск" >
<input type="text" id="input-search" name="q" placeholder="Поиск по всему сайту" />


</form>
</div>

</div>
<div id="top-menu">
	<ul>
		<li><img src="../images/shop.png" alt=""><a href="index.php">Главная</a></li>
		<li><img src="../images/new-32.png" alt=""><a href="">Новинки</a></li>
		<li><img src="../images/bestprice-32.png" alt=""><a href="">Лидеры продаж</a></li>
		<li><img src="../images/sale-32.png" alt=""><a href="">Распродажа</a></li>
	</ul>
	<p align="right" id="block-basket"><img src="../images/cart-icon.png" alt=""><a id="korzina" href="">Корзина пуста</a></p>
	<div id="nn"></div>
</div>